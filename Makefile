start-local:
	sudo docker-compose -f docker-compose-local.yml up -d

stop-local:
	sudo docker-compose -f docker-compose-local.yml stop

start-prod:
	sudo docker-compose -f docker-compose-prod.yml up -d

stop-prod:
	sudo docker-compose -f docker-compose-prod.yml stop