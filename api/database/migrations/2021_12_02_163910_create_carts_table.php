<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id')->default(0);
            $table->bigInteger('product_id');
            $table->integer('quantity');
            $table->double('unit_price',20,2)->default(0);
            $table->double('total',20,2)->default(0);
            $table->timestamps();
            $table->foreign('carts.order_id')->references('orders.id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
