<?php

declare(strict_types=1);

namespace Ideasoft\System;

class ResponseDataObject {
    protected object $data;

    function __construct() {
        $this->data = (object) [];
        $this->data->success = null;
        $this->data->message = null;
    }

    public function set($key, $value) {
        $this->data->{$key} = $value;
    }

    public function get() {
        return $this;
    }

    public function v1() : array {
        return [
            'meta' => [
                'success' => $this->data->success,
            ],
            'resource' => [
                'errorMessage' => $this->data->message
            ],
        ];
    }

    public function v2() : object {
        return (object) [];
    }
}
