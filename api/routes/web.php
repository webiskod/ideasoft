<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Settings\AccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Login Routes
 * */



Route::post("/login", [\App\Http\Controllers\Login\LoginController::class, "index"])->name("login");

Route::get("/order", [\App\Http\Controllers\Orders\OrderController::class, "getOrders"])->name("get-orders");
Route::post("/order", [\App\Http\Controllers\Orders\OrderController::class, "addOrder"])->name("add-order");
Route::delete("/order", [\App\Http\Controllers\Orders\OrderController::class, "deleteOrder"])->name("delete-order");

Route::post("/cart", [\App\Http\Controllers\Cart\CartController::class, "addCart"])->name("add-cart");
