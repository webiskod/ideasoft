<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        require_once str_replace("".DIRECTORY_SEPARATOR."Providers","".DIRECTORY_SEPARATOR."Http".DIRECTORY_SEPARATOR."functions.php",__DIR__);
    }
}
