<?php
namespace App\Helpers;
use App\Library\Http\HttpManager;
//use http\Env\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CommonHelper
{
    public static function getCategoryListing()
    {
        $httpManager = new HttpManager();
        $result =  $httpManager->get("menus/category",
            [
                'filter' => ['is_deleted' => 0],
                'select' => ['category_code', 'name'],
                'limit' => 0
            ]
        )->getResult();
        $categoryLevel = $result['resource']['data'] ?? $result['meta']['status'];
        return $categoryLevel;
    }

    public static function getMenuItem()
    {
        $httpManager = new HttpManager();
        $result =  $httpManager->get("menus/menu_item",
            [
                'filter' => ['is_deleted' => 0],
                'select' => ['menu_item_code', 'menu_item_name'],
                'limit' => 0
            ]
        )->getResult();
        $itemLevel = $result['resource']['data'] ?? $result['meta']['status'];
        return $itemLevel;
    }

    public static function getRestaurantCode()
    {
        $session = app('request')->session()->all();
        return $session['user_details']['restaurant_code'];
    }

    public static function getMenuType(){
        /*if($request->session()->exists('menu_type')){
            $menuType = $request->session()->get('menu_type');
        }else{
            $user_settings = $request->session()->get('user_details');
            $request->session()->put('menu_type', $user_settings['restaurant_users'][0]['default_menu_type']);
            $request->session()->put('default_menu_type', $user_settings['restaurant_users'][0]['default_menu_type']);
            $menuType = $user_settings['restaurant_users'][0]['default_menu_type'];
        }*/

        return request()->session()->get('menu_type');
    }
}
