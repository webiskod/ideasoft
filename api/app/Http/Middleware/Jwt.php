<?php

namespace App\Http\Middleware;

use \Firebase\JWT\JWT;

class Jwt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorizationHeader = explode(' ',$request->header('Authorization'));
        $head = isset($authorizationHeader[0]) ? $authorizationHeader[0]: false;
        $jwt = isset($authorizationHeader[1]) ? $authorizationHeader[1]: false;

        if(!$head || !$jwt){
            return response()->json([
                'status' => 0,
                'reply' => 'Geçersiz kullanıcı!'
            ]);
        }
        try{
            $secretKey = env('JWT_SECRET');
            $decoded = JWT::decode($jwt, $secretKey, array('HS256'));

            $request->attributes->add(['decoded' => $decoded, 'jwt' => $jwt]);
            return $next($request);
        } catch (ExpiredException $e) {
            return response()->json([
                'status' => 0,
                'reply' => 'Süresi dolmuş token!'
            ], 400);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'reply' => 'Geçersiz Kullanıcı!'
            ], 400);
        }
    }
}
