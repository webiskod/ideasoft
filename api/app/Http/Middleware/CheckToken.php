<?php

namespace App\Http\Middleware;

use App\Library\Http\HttpManager;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use PHPUnit\Util\PHP\AbstractPhpProcess;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /*if(isset(request()->session()->all()["user_details"]) AND (request()->session()->all()["user_details"]["settings_active"] ?? 0) == 0 AND request()->segment(1) != "setting")
        {
            return redirect('setting/restaurant');
        }*/
        if (isset($request->session()->all()["user_details"]["restaurant_users"]["language_code"])) {
            App::setLocale($request->session()->all()["user_details"]["restaurant_users"]["language_code"]);
        }

        if (!$request->session()->has('token')) {
            return redirect('login');
        }

        $http = new HttpManager();
        $http->get('me');
        $result = $http->getResult();

        if (isset($result["meta"])) {

            if ($result["meta"]["status"] === 401) {
                $request->session()->flush();
                return redirect('login');
            }
        }

        $http = new HttpManager(2);
        $http->post('core/user/auth');
        $result = $http->getResult();

        if(isset($result['success']) === false || $result['success'] === false) {
            $request->session()->flush();
            return redirect('login');
        }

        unset($http);
        unset($result);

        return $next($request);
    }
}
