<?php

declare(strict_types=1);

namespace App\Http\Traits;

trait ProductTrait {
    public function getProducts() {
        return $this->hasMany('\App\Http\Models\Products', 'product_id', 'id');
    }
}