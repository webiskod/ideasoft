<?php

declare(strict_types=1);

namespace App\Http\Traits;

trait CartTrait {
    public function getCart() {
        // return 1111;
        return $this->hasMany(\App\Http\Model\Cart\Cart::class, 'order_id', 'id')->getResults();
    }
}