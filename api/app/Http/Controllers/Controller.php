<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function postValues($except = array(), $file = null)
    {
        $postValues = app('request')->post();


        if (isset($postValues["datas"])) {
            $postValues = $postValues['datas'];
        }

        foreach ($except as $key => $value) {
            if (isset($postValues[$key])) {
                unset($postValues[$key]);
            }
        }

        if (isset($postValues['_token'])) {
            unset($postValues['_token']);
        }

        $file = $file . '_image';

        if (isset($_FILES[$file]) && $_FILES[$file]['error'] == '0') {

            $filename = basename($_FILES[$file]['name']);

            $files = [
                $file => curl_file_create(
                    $_FILES[$file]['tmp_name'],
                    $_FILES[$file]['type'],
                    $filename

                )

            ];
            foreach ($postValues as $k => $v) {
                if ($postValues[$k] == NULL) {
                    $postValues[$k] = "";
                }
            }
            $postValues = array_merge($postValues, $files);
        }

        return $postValues;
    }
    public function postValuesMultiFile($except = array(), $file = null)
    {
        $postValues = app('request')->post();


        if (isset($postValues["datas"])) {
            $postValues = $postValues['datas'];
        }

        foreach ($except as $key => $value) {
            if (isset($postValues[$key])) {
                unset($postValues[$key]);
            }
        }

        if (isset($postValues['_token'])) {
            unset($postValues['_token']);
        }
        if (is_array($file)) {
            foreach ($file as $key => $item) {
                if (isset($_FILES[$item]) && $_FILES[$item]['error'] == '0') {

                    $filename = basename($_FILES[$item]['name']);

                    $files = [
                        $item => curl_file_create(
                            $_FILES[$item]['tmp_name'],
                            $_FILES[$item]['type'],
                            $filename

                        )

                    ];
                    foreach ($postValues as $k => $v) {
                        if ($postValues[$k] == NULL) {
                            $postValues[$k] = "";
                        }
                    }
                    $postValues = array_merge($postValues, $files);
                }
            }
        } else {
        }


        return $postValues;
    }
    public function postValuesImage($except = array(), $file = null)
    {
        $postValues = app('request')->post();


        if (isset($postValues["datas"])) {
            $postValues = $postValues['datas'];
        }

        foreach ($except as $key => $value) {
            if (isset($postValues[$key])) {
                unset($postValues[$key]);
            }
        }

        if (isset($postValues['_token'])) {
            unset($postValues['_token']);
        }

        $file = $file;

        if (isset($_FILES[$file]) && $_FILES[$file]['error'] == '0') {

            $filename = basename($_FILES[$file]['name']);

            $files = [
                $file => curl_file_create(
                    $_FILES[$file]['tmp_name'],
                    $_FILES[$file]['type'],
                    $filename

                )

            ];
            foreach ($postValues as $k => $v) {
                if ($postValues[$k] == NULL) {
                    $postValues[$k] = "";
                }
            }
            $postValues = array_merge($postValues, $files);
        }

        return $postValues;
    }

    public function getUserCurrentPageChannel($key)
    {
        if(!isset(request()->session()->get("user_details")["restaurant_users"][0]["user_code"]))
            redirect(route("logout"));

        $key = request()->session()->get("user_details")["restaurant_users"][0]["user_code"].$key;
        return md5($key);
    }

}
