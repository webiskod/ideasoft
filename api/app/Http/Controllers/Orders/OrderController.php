<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Cart\CartController;
use App\Http\Controllers\Controller;
use App\Http\Model\Orders\Orders;

class OrderController extends Controller
{
    public function getOrders()
    {
        $orders = Orders::getOrders();
        $orderList = [];
        foreach ($orders AS $order) {
            $cartData = collect($order->getCart())->toJson();
            $orderList[] = [
              'id'=>$order->id,
              'customerId'=>$order->customer_id,
              'items'=>$cartData,
              'total'=>$order->total
            ];
        }

        return $orderList;
    }

    public function addOrder()
    {
        return Orders::addOrder();
    }

    public function deleteOrder()
    {
        $data = request()->only('id');
        if(!isset($data['id'])){
            return [
                "meta"=> [
                    "success"=>false
                ],
                "resource"=> [
                    "errorMessage"=>"Geçersiz İşlem"
                ]
            ];
        }
        return Orders::deleteOrder($data['id']);
    }


}
