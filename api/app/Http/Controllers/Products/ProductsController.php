<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Http\Model\Products\Products;

class ProductsController extends Controller
{
    public function getProducts()
    {
        $products = (new \App\Http\Model\Products\Products)->getProducts();

        return $products;
    }

    public function addProduct($data)
    {
        return (new \App\Http\Model\Products\Products)->addProduct($data);
    }

}
