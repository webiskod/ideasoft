<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use App\Http\Model\Cart\Cart;

class CartController extends Controller
{
    public static function addCart()
    {
        $data = request()->only('product_id','quantity');
        return Cart::addCart($data);
    }
}
