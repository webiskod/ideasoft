<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use App\Http\Middleware\Authenticate;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        $loginData = request()->only('email','password');
        if(Auth::attempt($loginData)){
            $jwt = JWT::encode([
                'name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'iat' => time(),
                "exp" => time() + 60 * 60 * 1
        ], env('API_KEY'));
            return [
                "meta"=> [
                    "success"=>true
                ],
                "resource"=> [
                    "JWT"=>$jwt
                ]
            ];
        }

        return [
        "meta"=> [
            "success"=>false
        ],
        "resource"=> [
            "errorMessage"=>"Geçersiz Kullanıcı"
        ]
    ];

    }
}
