<?php

namespace App\Http\Library\Wrappers;

use Predis\Client;

class RedisWrapper {
    private static $_instance = null;
    private $_client = null;

    private function __construct() {
        # Get Redis Config
        $redisConfig = (array) conf('redis');

        # Instantiate Redis Client
        $this->_client = new Client($redisConfig);
    }

    public static function getClient(): Client {
        if (self::$_instance === null) {
            self::$_instance = new RedisWrapper();
        }

        return self::$_instance->_client;
    }
}
