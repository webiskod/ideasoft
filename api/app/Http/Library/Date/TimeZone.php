<?php


namespace App\Http\Library\Date;


class TimeZone
{
    protected $timezone=null;
    public function __construct()
    {
        $this->timezone = trim(request()->session()->get("user_detail")["timezoneString"] ?? null);

        if(is_null($this->timezone) || $this->timezone === "")
            $this->timezone=env("DEFAULT_TIMEZONE","America/New_York");

    }


    public function getTimeZone(){
        return $this->timezone;
    }

    public function setTimeZone($timeZone){
        $this->timezone = $timeZone;
        return $this->timezone;
    }

    public function setDefaultTimeZone(){
        date_default_timezone_set($this->getTimeZone());
    }
}
