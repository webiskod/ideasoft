<?php

namespace App\Http\Model\Products;

use App\Http\Model\Orders\Cart;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public static function deleteProduct($id)
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    /**
     * @param int $pagination
     * @param array $filter
     * @return mixed
     *
     */
    public function getProducts($pagination = 15,$filter = array())
    {
        $products = DB::table("products");
        if(count($filter) > 0){
            foreach($filter as $k=>$v){
                $products->where($k,"=",$v);
            }
        }
        return $products
            ->select('id','name','category','price','stock')
            ->paginate($pagination)
            ->toArray();
    }

    /**
     * @param $data
     * @return mixed
     *
     */
    public function addProduct($data)
    {
        DB::beginTransaction();
        try {

            DB::table("products")
                ->insertGetId($data);

        }catch (\Exception $e){
            DB::rollBack();
            return $e;
        }

        DB::commit();

        return true;
    }
}
