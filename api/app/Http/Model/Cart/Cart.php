<?php

namespace App\Http\Model\Cart;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
    public function getCart()
    {
        return DB::table("carts")
            ->where("user_id",Auth::user()->id)
            ->where("order_id",0)
            ->leftJoin('products','products.id','=','carts.product_id')
            ->select('carts.*','products.category')
            ->orderBy('total')
            ->paginate(99)
            ->toArray();
    }

    public function addCart($cart)
    {
        DB::beginTransaction();

        $cart = \App\Http\Model\Cart\Cart::getCart();
        if(!stockControl($cart)){
            return (object) [
                "products"=>[
                    "stock"=>"Yeterli Stok Yok!"
                ]
            ];
        }

        try {
            foreach($cart as $c){
                $c["order_id"]=0;
                DB::table("carts")
                    ->insert($c);
            }
        }catch (\Exception $e){
            DB::rollBack();
            return $e;
        }
        DB::commit();
        return true;
    }
}
