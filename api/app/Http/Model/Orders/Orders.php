<?php

namespace App\Http\Model\Orders;

use App\Http\Traits\CartTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

function stockControl($cart)
{
    if(($cart["total"] ?? 0) == 0){
        return false;
    }
    foreach($cart["data"] as $c){
        if(DB::table('products')->where('id', $c->product_id)->value('stock') < $c->stock){
            return false;
        }
    }
    return true;
}

function discountControl($cart)
{
    if(($cart["total"] ?? 0) == 0){
        return 0;
    }
    $category = $discounts = [];
    $categoryPrice = $cCount = $byCategories = $total=0;
    foreach($cart["data"] as $c){
        $category[$c->category][] = $c;
        $total += $c->total;
        if($c->category == 2){
            $byCategories++;
            $categoryPrice = $c->total;
        }
    }

    if($total >= 1000){
        $discounts[] = (object)[
            "discountReason"=> "10_PERCENT_OVER_1000",
            "discountAmount"=> $total * 0.1,
            "subtotal"=> $total - $total * 0.1
        ];

    }
    if($byCategories >= 6){
        if($cCount++ == 0){
            $discounts[] = (object)[
                "discountReason"=> "10_PERCENT_OVER_1000",
                "discountAmount"=> $categoryPrice,
                "subtotal"=> $total - $categoryPrice
            ];
        }
    }

    if(count($category[1]) >= 2){
        $discounts[] = (object)[
            "discountReason"=> "20_PERCENT_CATEGORY_1",
            "discountAmount"=> $category[1][0]->total * 0.2,
            "subtotal"=> $total - $category[1][0]->total * 0.2
        ];
    }


    return $discounts;
}

class Orders extends Model
{
    use CartTrait;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cart()
    {
        return $this->hasMany(Cart::class)->getResults();
    }

    /**
     * @param int $pagination
     * @param array $filter
     * @return mixed
     *
     */
    public static function getOrders($pagination = 15,$filter = array())
    {
        return self::all();



        $products = DB::table("orders");
        if(count($filter) > 0){
            foreach($filter as $k=>$v){
                $products->where($k,"=",$v);
            }
        }
        return $products
            ->paginate($pagination)
            ->toArray();
    }

    /**
     * @param $order
     * @return mixed
     *
     */
    public static function addOrder($order)
    {
        DB::beginTransaction();

        $userCart = \App\Http\Model\Cart\Cart::getCart();
        if(!stockControl($userCart)){
            return (object) [
                "products"=>[
                    "stock"=>"Yeterli Stok Yok!"
                ]
            ];
        }

        if(($userCart["total"] ?? 0) == 0){
            return (object) [
                "cart"=>[
                    "item"=>"Sipariş verebilmeniz için lütfen sepete ürün ekleyiniz"
                ]
            ];
        }

        $discount = discountControl($userCart);
        $order["discount_detail"] = json_encode($discount);

        if(count($discount)>0){
            foreach ($discount as $d){
                $order['discount'] = ($order['discount'] ?? 0) + $d['discountAmount'];
            }
        }

        try {
            $lastOrderId = DB::table("orders")
                ->insertGetId($order);

            if($lastOrderId > 0){
                DB::table("cart")
                    ->where("user_id",Auth::user()->id)
                    ->where("order_id",0)
                    ->update(["order_id"=>$lastOrderId]);
            }

        }catch (\Exception $e){
            DB::rollBack();
            return $e;
        }

        DB::commit();

    }


    /**
     * @param $id
     * @return bool|\Exception
     */
    public static function deleteOrder($id)
    {
        DB::beginTransaction();
        try {

            DB::table("orders")
                ->where("id",$id)
                ->update(["is_deleted"=>1]);

        }catch (\Exception $e){
            DB::rollBack();
            return $e;
        }

        DB::commit();
        return true;
    }


}
