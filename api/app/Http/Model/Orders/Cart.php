<?php

namespace App\Http\Model\Orders;

use App\Http\Model\Products\Products;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function orders()
    {
        return $this->belongsTo(Orders::class);
    }

    public function products()
    {
        return $this->hasMany(Products::class);
    }
}
