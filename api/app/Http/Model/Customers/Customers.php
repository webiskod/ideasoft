<?php

namespace App\Http\Model\Customers;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    /**
     * @param int $pagination
     * @param array $filter
     * @return mixed
     *
     */
    public function getCustomers($pagination = 15,$filter = array())
    {
        $customers = DB::table("customers");
        if(count($filter) > 0){
            foreach($filter as $k=>$v){
                $customers->where($k,"=",$v);
            }
        }
        return $customers
            ->select('id','name','since','revenue')
            ->paginate($pagination)
            ->toArray();
    }

    /**
     * @param $data
     * @return mixed
     *
     */
    public function addCustomer($data)
    {
        DB::beginTransaction();
        try {

            DB::table("customers")
                ->insertGetId($data);

        }catch (\Exception $e){
            DB::rollBack();
            return $e;
        }

        DB::commit();

        return true;
    }
}
