<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherOperation extends Model
{
    use HasFactory;
    public static function convertToString($value, $type)
    {
        $newArray = [];
        $result = '';
        if ($type == 'stringArray') {
            $value = json_decode($value, 1);
            if (is_array($value) && count($value)) {
                foreach ($value as $item) {
                    $newArray[] = ucwords(str_replace('_', ' ', $item));
                }
                if (is_array($newArray)) {
                    $result = implode(', ', $newArray);
                }
            }
        } elseif ($type == "string") {
        } else {
            if (is_array($value) && count($value)) {
                foreach ($value as $item) {
                    $newArray[] = ucwords(str_replace('_', ' ', $item));
                }
                if (is_array($newArray)) {
                    $result = implode(', ', $newArray);
                }
            }
        }
        return $result;
    }
}
