<?php

namespace App\Library\Http;

use ideasoft\System\ResponseDataObject;
use Illuminate\Support\Facades\Session;

/**
 * Class HttpManager
 * @package App\Library\Curl
 */
class HttpManager
{
    /**
     * @var string
     */
    protected $url = null;
    protected $stripeBase = null;
    protected $stripeAuth = null;
    protected $urlV2 = null;
    protected $curlInfo = null;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var int
     */
    protected $version = null;

    /**
     * @var null|mixed
     */
    protected $result;

    /**
     * get http code
     *
     * @return null|mixed
     */

    public function __construct($version = 1)
    {

    }

    public function code()
    {
        if (is_array($this->result)) {
            return $this->result['meta']['status'];
        }

        return null;
    }

    /**
     * get error message
     *
     * @return null|mixed
     */
    public function errorMessage()
    {
        $resource = $this->resource();

        if (is_array($this->result) && isset($resource['errorMessage'])) {
            return $resource['errorMessage'];
        }

        return null;
    }

    /**
     * post request for http
     *
     * @param null|string $path
     * @param array $data
     * @param array $header
     * @return self
     */
    public function get($path, $data = [])
    {

    }

    public function addHeader($header)
    {
        $this->headers[] = $header;
    }
}
